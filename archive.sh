rm -rf upload/*

xcodebuild \
-workspace DemoProject.xcworkspace \
-scheme DemoProject \
-archivePath upload/DemoProject.xcarchive \
archive | xcpretty

xcodebuild  -exportArchive \
-archivePath upload/DemoProject.xcarchive \
-exportPath upload/DemoProject.ipa \
-exportOptionsPlist hoc.plist | xcpretty
