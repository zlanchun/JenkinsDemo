//
//  PZTagListCollectionViewFlowLayout.m
//  DemoProject
//
//  Created by z on 2017/9/27.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "PZTagListCollectionViewFlowLayout.h"

@implementation PZTagListCollectionViewFlowLayout



- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *attributesForElementsInRect = [super layoutAttributesForElementsInRect:rect];
    
    NSMutableArray *newAttributesForElementsInRect = [@[] mutableCopy];
    // use a value to keep track of left margin
    CGFloat originX = 0.0;
    CGFloat margin = 8;
    for (UICollectionViewLayoutAttributes *attributes in attributesForElementsInRect) {
        
        NSLog(@"attributes: %f, sectionInset.left: %f, width: %f",attributes.frame.origin.x,self.sectionInset.left,attributes.frame.size.width);
        // assign value if next row
        if (attributes.frame.origin.x == self.sectionInset.left) {
            originX = self.sectionInset.left;
        } else {
            // set x position of attributes to current margin
            CGRect newLeftAlignedFrame = attributes.frame;
            newLeftAlignedFrame.origin.x = originX;
            attributes.frame = newLeftAlignedFrame;
        }
        // calculate new value for current margin
        originX += attributes.frame.size.width + margin;
        [newAttributesForElementsInRect addObject:attributes];
    }
    return newAttributesForElementsInRect;
}

@end
