//
//  CustomCell.m
//  DemoProject
//
//  Created by z on 2017/9/27.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "CustomCell.h"
#import <Masonry/Masonry.h>


@implementation CustomCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:75/255.0 green:173/255.0 blue:107/255.0 alpha:1];
        self.layer.cornerRadius = 3;
        
        self.titleLabel = ({
            UILabel *tmpLabel = [[UILabel alloc] init];
            tmpLabel.textColor = [UIColor whiteColor];
            tmpLabel.font = [UIFont systemFontOfSize:15];
            tmpLabel.textAlignment = NSTextAlignmentCenter;
            tmpLabel;
        });
        
        [self.contentView addSubview:self.titleLabel];
        
        UIEdgeInsets edge = UIEdgeInsetsMake(8, 8, 8, 8);
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(edge);
        }];
    }
    return self;
}


@end
