//
//  ViewController.m
//  DemoProject
//
//  Created by z on 2017/9/22.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"
#import "CustomCell.h"
#import <Masonry/Masonry.h>
#import "PZTagListCollectionViewFlowLayout.h"




@interface ViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSString *varName;
@end

@implementation ViewController {
    NSArray *titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titles = @[@"Tech", @"Design", @"Humor", @"Travel", @"Music", @"Writing", @"Social Media", @"Life", @"Education", @"Edtech", @"Education Reform", @"Photography", @"Startup", @"Poetry", @"Women In Tech", @"Female Founders", @"Business", @"Fiction", @"Love", @"Food", @"Sports"];
    
    /*
     Demo 效果
     
     clang：
        少输入 ->
            年、月、日
            内容 -> push
        显示 -》
            剩余时间
            自己、他人、
            
     releation
     
     real
     
     life
     
     true or false 
     
     the water on the time
     
     moon of night
     
     */
    
    PZTagListCollectionViewFlowLayout *flowLayout = [PZTagListCollectionViewFlowLayout new];
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 0);
    flowLayout.estimatedItemSize = CGSizeMake(20, 20);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[CustomCell class] forCellWithReuseIdentifier:@"CustomCell"];
    
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuide);
    }];
    
    NSLog(@"height : %f",self.collectionView.bounds.size.height);
}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//                  layout:(UICollectionViewLayout *)collectionViewLayout
//  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    CustomCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    return [cell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return titles.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCell" forIndexPath:indexPath];
    cell.titleLabel.text = titles[indexPath.row];
    return cell;
}

- (void)foo {
    NSLog(@"%@",self);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
