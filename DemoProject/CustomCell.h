//
//  CustomCell.h
//  DemoProject
//
//  Created by z on 2017/9/27.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell
@property (nonatomic,strong) UILabel *titleLabel;
@end
