rm -rf upload/*

set -o pipefail && \
xcodebuild -scheme 'DemoProject' \
-workspace 'DemoProject.xcworkspace' \
-configuration 'Debug' \
-archivePath upload/DemoProject.xcarchive \
archive | xcpretty

xcodebuild  -exportArchive \
-exportOptionsPlist hoc.plist \
-archivePath upload/DemoProject.xcarchive \
-exportPath upload/DemoProject.ipa 

